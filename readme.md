## About Hierarchy project

Version 0.0.1

Just upload a text file with employee hierarchy and see how it looks.

### Installation

#### For production
* Run:
```
composer install -o --no-dev
```

#### For development
* Run:
```
composer install -o
nvm install
nvm use
npm install
npm run dev
```