<?php


use Illuminate\Http\UploadedFile;

class EmployeeTest extends TestCase
{
    /**
     * Provides paths to test files
     *
     * @return string[]
     */
    public function fileProvider(): array
    {
        if (!$this->app) {
            $this->refreshApplication();
        }

        $basePath = $this->app->basePath('storage/tmp');

        $this->test_files = [
            'json' => $basePath . DIRECTORY_SEPARATOR . 'test.json',
            'txt'  => $basePath . DIRECTORY_SEPARATOR . 'test.txt',
        ];

        return $this->makeIterable($this->test_files);
    }

    /**
     * Test uploading a file with hierarchy and getting structured information
     *
     * @dataProvider fileProvider
     *
     * @param string $fullFilePath
     *
     * @return void
     */
    public function testPostUploadHierarchyFile(string $fullFilePath): void
    {
        // defining temporary files so they would be removed after the test in tearDown()
        $this->test_files = [$fullFilePath];

        $records = $this->getEmployeeRecords();

        $json = json_encode($records['test']);

        file_put_contents($fullFilePath, $json);

        $ext = pathinfo($fullFilePath)['extension'];
        $mimeType  = $ext === 'json' ? 'application/json' : ($ext === 'txt' ? 'text/plain' : null);

        $file = new UploadedFile(
            $fullFilePath,
            pathinfo($fullFilePath)['basename'],
            $mimeType,
            filesize($fullFilePath),
            null,
            true
        );

        $response = $this->call(
            'POST',
            'api/v1/employee/hierarchy/file',
            [],
            [],
            ['hierarchy_file' => $file],
            [
                'Accept'           => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
            ]
        );

        $this->assertEquals(200, $response->status());

        $this->assertJson($response->content());

        $this->assertJsonStringEqualsJsonString(
            $response->content(),
            json_encode([
                'success' => true,
                'data'    => $records['result'],
            ])
        );
    }

    /**
     * Provides records for testing
     *
     * @return array
     */
    protected function getEmployeeRecords(): ?array
    {
        return [
            'test' => [
                'Pete'    => 'Nick',
                'Barbara' => 'Nick',
                'Sophie'  => 'Jonas',
                'Nick'    => 'Sophie',
                'Cris'    => 'Sophie',
            ],

            'result' => [
                'Jonas' => [
                    [
                        'Sophie' => [
                            [
                                'Nick' => [
                                    ['Pete' => []],
                                    ['Barbara' => []],
                                ],
                            ],
                            [
                                'Cris' => [],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
