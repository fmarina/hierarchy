<?php

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    protected $test_files = [];

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    /**
     * Execute after each test
     */
    public function tearDown()
    {
        foreach ($this->test_files as $filePath) {
            unlink($filePath);
        }

        Mockery::close();

        parent::tearDown();
    }

    /**
     * Get an array of iterable data sets.
     *
     * @param array $data - a flat array
     * @return array - an iterable data set
     */
    public function makeIterable(array $data): array
    {
        // each data set should be iterable, so we wrap each element with an array
        return array_map('array_wrap', $data);
    }

    /**
     * Get combined data set from multiple providers
     *
     * @param $arrays
     * @return array
     */
    public function getCombinations(...$arrays): array
    {
        $result = [[]];

        foreach ($arrays as $property => $property_values) {
            $tmp = [];
            foreach ($result as $result_item) {
                foreach ($property_values as $property_value) {
                    $tmp[] = array_merge($result_item, [$property => $property_value[0]]);
                }
            }
            $result = $tmp;
        }
        return $result;
    }
}
