<?php


use App\Api\v1\Traits\DecodesJson;

class DecodesJsonTest extends TestCase
{
    /**
     * Provides paths to test files
     *
     * @return string[]
     */
    public function fileProvider(): array
    {
        if (!$this->app) {
            $this->refreshApplication();
        }

        $basePath = $this->app->basePath('storage/tmp');

        $test_files = [
            'json' => $basePath . DIRECTORY_SEPARATOR . 'test.json',
            'txt' => $basePath . DIRECTORY_SEPARATOR . 'test.txt',
        ];

        return $this->makeIterable($test_files);
    }

    /**
     * Test decodeJson method
     *
     * @covers \App\Api\v1\Traits\DecodesJson::decodeJson
     */
    public function testDecodeJson(): void
    {
        /** @var DecodesJson $trait */
        $trait = $this->getMockForTrait(DecodesJson::class);

        $records = $this->getJsonRecords();

        $json = json_encode($records);

        $result = $trait->decodeJson($json);

        $this->assertEquals($result, $records);
    }

    /**
     * Test decodeJsonFromFile method
     *
     * @dataProvider fileProvider
     *
     * @param string $filePath
     */
    public function testDecodeJsonFromFile(string $filePath): void
    {
        // defining temporary files so they would be removed after the test in tearDown()
        $this->test_files = [$filePath];

        /** @var DecodesJson $trait */
        $trait = $this->getMockForTrait(DecodesJson::class);

        $records = $this->getJsonRecords();

        $json = json_encode($records);
        file_put_contents($filePath, $json);

        $result = $trait->decodeJsonFromFile($filePath);

        $this->assertEquals($result, $records);
    }

    /**
     * Test decodeJsonFromFile method
     *
     * @dataProvider fileProvider
     *
     * @param string $filePath
     */
    public function testDecodeJsonFromFileWhichNotExist(string $filePath): void
    {
        /** @var DecodesJson $trait */
        $trait = $this->getMockForTrait(DecodesJson::class);

        $result = $trait->decodeJsonFromFile($filePath);

        $this->assertEquals($result, null);
    }

    /**
     * Provides records for testing
     *
     * @return array
     */
    protected function getJsonRecords(): array
    {
        return [
            'Pete'    => 'Nick',
            'Barbara' => 'Nick',
            'Sophie'  => 'Jonas',
            'Nick'    => 'Sophie',
            'Cris'    => 'Sophie',
        ];
    }
}