<?php

use App\Api\v1\Managers\Employee\Hierarchy\HierarchyException;
use App\Api\v1\Managers\Employee\Hierarchy\HierarchyManagerInterface;
use App\Api\v1\Managers\Employee\Hierarchy\SimpleHierarchyManager;

class HierarchyManagerTest extends TestCase
{

    /**
     * Provides classes that implement HierarchyManagerInterface
     * for testing public methods
     *
     * @return HierarchyManagerInterface[]
     */
    public function managerProvider(): array
    {
        $classes = [
            SimpleHierarchyManager::class,
        ];

        return $this->makeIterable($classes);
    }

    /**
     * Provides paths to test files
     *
     * @return string[]
     */
    public function fileProvider(): array
    {
        if (!$this->app) {
            $this->refreshApplication();
        }

        $basePath = $this->app->basePath('storage/tmp');

        $test_files = [
            'json' => $basePath . DIRECTORY_SEPARATOR . 'test.json',
            'txt' => $basePath . DIRECTORY_SEPARATOR . 'test.txt',
        ];

        return $this->makeIterable($test_files);
    }

    /**
     * Provides combinations of test files and managers
     *
     * @return array[]
     */
    public function managerAndFileProvider(): array
    {
        $files = $this->fileProvider();
        $managers = $this->managerProvider();

        return $this->getCombinations($managers, $files);;
    }

    /**
     * Provides names for testing
     *
     * @return array[]
     */
    public function nameProvider(): array
    {
        return $this->makeIterable(['Nick', 'Pete', 'Barbara', 'Cris']);
    }

    /**
     * Test isDependent method
     *
     * @dataProvider nameProvider
     *
     * @param string $name
     * @return void
     * @throws ReflectionException
     */
    public function testIsDependent(string $name): void
    {
        $test_array = $this->getEmployeeRecords('correct')['result'];

        $manager = new SimpleHierarchyManager;

        $reflectionMethod = new ReflectionMethod(SimpleHierarchyManager::class, 'isDependent');
        $reflectionMethod->setAccessible(true);

        $isDependent = $reflectionMethod->invoke($manager, $name, $test_array['Jonas']);

        $this->assertTrue($isDependent);
    }

    /**
     * Test isDependent method to return false
     *
     * @dataProvider nameProvider
     *
     * @return void
     * @throws ReflectionException
     */
    public function testIsDependentFalse(): void
    {
        $test_array = $this->getEmployeeRecords('correct')['result'];

        $manager = new SimpleHierarchyManager;

        $reflectionMethod = new ReflectionMethod(SimpleHierarchyManager::class, 'isDependent');
        $reflectionMethod->setAccessible(true);

        $isDependent = $reflectionMethod->invoke($manager, 'Jack', $test_array['Jonas']);

        $this->assertFalse($isDependent);
    }

    /**
     * Test buildTreeFromArray method
     *
     * @dataProvider managerProvider
     * @param string $manager_class
     * @return void
     * @throws HierarchyException
     */
    public function testBuildTreeFromArray(string $manager_class): void
    {
        /** @var HierarchyManagerInterface $manager */
        $manager = new $manager_class;

        $records = $this->getEmployeeRecords('correct');

        $hierarchy = $manager->buildTreeFromArray($records['test']);

        $this->assertEquals(
            $hierarchy, $records['result']
        );
    }

    /**
     * Test buildTreeFromArray method if there is a loop
     *
     * @dataProvider managerProvider
     * @param string $manager_class
     * @return void
     * @throws HierarchyException
     */
    public function testBuildTreeFromArrayWithLoop(string $manager_class): void
    {
        $this->expectExceptionMessage(HierarchyException::class);
        $this->expectExceptionMessage('Hey! No loops please!');

        /** @var HierarchyManagerInterface $manager */
        $manager = new $manager_class;

        $records = $this->getEmployeeRecords('with_loop');

        $manager->buildTreeFromArray($records);
    }

    /**
     * Test buildTreeFromArray method if there is more than on big Boss
     *
     * @dataProvider managerProvider
     * @param string $manager_class
     * @return void
     * @throws HierarchyException
     */
    public function testBuildTreeFromArrayWithManyHeads(string $manager_class): void
    {
        $this->expectExceptionMessage(HierarchyException::class);
        $this->expectExceptionMessage('Hey! There was supposed to be just one big Boss!');

        /** @var HierarchyManagerInterface $manager */
        $manager = new $manager_class;

        $records = $this->getEmployeeRecords('with_many_heads');

        $manager->buildTreeFromArray($records);
    }

    /**
     * Test buildTreeFromArray method if there is a wrong format
     *
     * @dataProvider managerProvider
     * @param string $manager_class
     * @return void
     * @throws HierarchyException
     */
    public function testBuildTreeFromArrayWithWrongFormat(string $manager_class): void
    {
        $this->expectExceptionMessage(HierarchyException::class);
        $this->expectExceptionMessage('Hey! The records should contain only key:value structure!');

        /** @var HierarchyManagerInterface $manager */
        $manager = new $manager_class;

        $records = $this->getEmployeeRecords('wrong_format');

        $manager->buildTreeFromArray($records);
    }

    /**
     * Test buildTreeFromArray method with no records passed
     *
     * @dataProvider managerProvider
     * @param string $manager_class
     * @return void
     * @throws HierarchyException
     */
    public function testBuildTreeFromArrayWithEmpty(string $manager_class): void
    {
        $this->expectExceptionMessage(HierarchyException::class);
        $this->expectExceptionMessage('There should be at list one record.');

        /** @var HierarchyManagerInterface $manager */
        $manager = new $manager_class;

        $manager->buildTreeFromArray([]);
    }

    /**
     * Test buildTreeFromFile method
     *
     * @dataProvider managerAndFileProvider
     *
     * @param string $manager_class
     * @param string $filePath
     *
     * @return void
     * @throws HierarchyException
     * @covers \App\Api\v1\Traits\DecodesJson::decodeJsonFromFile
     * @covers \App\Api\v1\Traits\DecodesJson::decodeJson
     */
    public function testBuildTreeFromFile(string $manager_class, string $filePath): void
    {
        // defining temporary files so they would be removed after the test in tearDown()
        $this->test_files = [$filePath];

        /** @var HierarchyManagerInterface $manager */
        $manager = new $manager_class;

        $records = $this->getEmployeeRecords('correct');

        $json = json_encode($records['test']);

        file_put_contents($filePath, $json);

        $hierarchy = $manager->buildTreeFromFile($filePath);

        $this->assertEquals(
            $hierarchy, $records['result']
        );
    }

    /**
     * Test buildTreeFromFile method with an empty file
     *
     * @dataProvider managerAndFileProvider
     *
     * @param string $manager_class
     * @param string $filePath
     *
     * @return void
     * @throws HierarchyException
     * @covers \App\Api\v1\Traits\DecodesJson::decodeJsonFromFile
     * @covers \App\Api\v1\Traits\DecodesJson::decodeJson
     */
    public function testBuildTreeFromFileWithEmptyFile(string $manager_class, string $filePath): void
    {
        $this->expectExceptionMessage(HierarchyException::class);
        $this->expectExceptionMessage('The file can not be read. Check your code here https://jsonlint.com/, for example.');

        // defining temporary files so they would be removed after the test in tearDown()
        $this->test_files = [$filePath];

        /** @var HierarchyManagerInterface $manager */
        $manager = new $manager_class;

        file_put_contents($filePath, '');

        $manager->buildTreeFromFile($filePath);
    }

    /**
     * Test buildTreeFromFile method with an empty file
     *
     * @dataProvider managerAndFileProvider
     *
     * @param string $manager_class
     * @param string $filePath
     *
     * @return void
     * @throws HierarchyException
     * @covers \App\Api\v1\Traits\DecodesJson::decodeJsonFromFile
     * @covers \App\Api\v1\Traits\DecodesJson::decodeJson
     */
    public function testBuildTreeFromFileWithEmptyArray(string $manager_class, string $filePath): void
    {
        $this->expectExceptionMessage(HierarchyException::class);
        $this->expectExceptionMessage('The file should contain JSON code with at list one record.');

        // defining temporary files so they would be removed after the test in tearDown()
        $this->test_files = [$filePath];

        /** @var HierarchyManagerInterface $manager */
        $manager = new $manager_class;

        file_put_contents($filePath, json_encode([]));

        $manager->buildTreeFromFile($filePath);
    }

    /**
     * Provides records for testing
     *
     * @param string $type
     * @return array|null
     */
    protected function getEmployeeRecords(string $type): ?array
    {
        if ($type === 'correct') {
            return [
                'test'   => [
                    'Pete'    => 'Nick',
                    'Barbara' => 'Nick',
                    'Sophie'  => 'Jonas',
                    'Nick'    => 'Sophie',
                    'Cris'    => 'Sophie',
                ],
                'result' => [
                    'Jonas' => [
                        [
                            'Sophie' => [
                                [
                                    'Nick' => [
                                        ['Pete' => []],
                                        ['Barbara' => []],
                                    ],
                                ],
                                [
                                    'Cris' => [],
                                ],
                            ],
                        ],
                    ],
                ],
            ];
        }

        if ($type === 'with_loop') {
            return [
                'Pete' => 'Nick',
                'Nick' => 'Mark',
                'Mark' => 'Ben',
                'Ben'  => 'Pete',
            ];
        }

        if ($type === 'with_many_heads') {
            return [
                'Pete' => 'Nick',
                'Nick' => 'Mark',
                'Mark' => 'Ben',
                'Ben'  => 'Barbara',
                'Jack' => 'Jonas',
            ];
        }

        if ($type === 'wrong_format') {
            return [
                'Pete' => 'Nick',
                'Nick' => 'Mark',
                'Mark' => 'Ben',
                'Ben'  => [],
            ];
        }

        return null;
    }
}
