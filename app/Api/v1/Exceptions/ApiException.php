<?php

namespace App\Api\v1\Exceptions;


use Exception;
use Throwable;

/**
 * Exception App\Api\v1\Exceptions\ApiException
 *
 * @package App\Api\v1
 * @codeCoverageIgnore
 */
class ApiException extends Exception
{
    /** @var array $errors */
    protected $errors;

    /** @var int $statusCode */
    protected $statusCode;

    /**
     * Create a new exception instance.
     *
     * @param string|null    $message
     * @param bool           $isError
     * @param array          $errors
     * @param int            $statusCode
     * @param Throwable|null $previous
     */
    public function __construct(
        string $message = null,
        bool $isError = false,
        array $errors = [],
        int $statusCode = 422,
        Throwable $previous = null)
    {
        $this->errors = $errors;
        $this->statusCode = $statusCode;

        if (null === $message) {
            $message = 'Something wrong with our API.';
        }

        if ($isError) {
            $this->errors[] = $message;
        }

        parent::__construct($message, $statusCode, $previous);
    }

    /**
     * Get all of the errors
     *
     * @return array
     */
    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * Add an errors
     *
     * @param $errors
     * @return void
     */
    public function addErrors($errors): void
    {
        foreach ($errors as $field => $error) {
            $this->errors[$field] = $error;
        }
    }

    /**
     * Get the response status
     *
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}