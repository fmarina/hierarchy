<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Responses\ApiResponse;
use App\Api\v1\Responses\ApiResponseInterface;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 *
 * @package App\Api\v1\Controllers
 */
class Controller extends BaseController
{
    /**
     * Return a new JSON response from the application.
     *
     * @param string|array $data
     * @param int          $status
     * @param array        $headers
     *
     * @return ApiResponseInterface
     */
    protected function response($data = [], $status = 200, array $headers = []): ApiResponseInterface
    {
        return ApiResponse::make($data, $status, $headers);
    }
}