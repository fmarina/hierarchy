<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Managers\Employee\Hierarchy\HierarchyException;
use App\Api\v1\Managers\Employee\Hierarchy\HierarchyManagerInterface;
use App\Api\v1\Responses\ApiResponseInterface;
use Illuminate\Http\Request;

/**
 * Class EmployeeController
 *
 * @package App\Api\v1\Controllers
 */
class EmployeeController extends Controller
{
    /**
     * Upload a file with json records and return structured information
     *
     * @param Request                   $request
     * @param HierarchyManagerInterface $manager
     *
     * @return ApiResponseInterface
     * @throws HierarchyException
     */
    public function postUploadHierarchyFile(Request $request, HierarchyManagerInterface $manager): ApiResponseInterface
    {
        $maxSize = (int) ini_get('upload_max_filesize') * 1000;

        $this->validate($request, [
            'hierarchy_file' => 'required|file|mimes:txt,json|max:' . $maxSize,
        ]);

        $filePath = $request->file('hierarchy_file')->getRealPath();

        $hierarchyTree = $manager->buildTreeFromFile($filePath);

        return $this->response([
            'success' => true,
            'data'   => $hierarchyTree,
        ]);
    }
}
