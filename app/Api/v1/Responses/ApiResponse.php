<?php

namespace App\Api\v1\Responses;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\JsonResponse;

/**
 * Class ApiResponse
 *
 * @package App\Api\v1\Responses
 */
class ApiResponse extends JsonResponse implements ApiResponseInterface
{
    /**
     * Return a new JSON response from the application.
     *
     * @param string|array|null $data
     * @param int               $status
     * @param array             $headers
     * @param int               $options
     *
     * @return ApiResponseInterface
     */
    public static function make($data = [], int $status = 200, array $headers = [], int $options = 0): ApiResponseInterface
    {
        if ($data instanceof Arrayable) {
            $data = $data->toArray();
        }

        return new static($data, $status, $headers, $options);
    }

}