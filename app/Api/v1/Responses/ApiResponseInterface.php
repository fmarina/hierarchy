<?php

namespace App\Api\v1\Responses;


/**
 * Interface ApiResponseInterface
 *
 * @package App\Api\v1\Responses
 */
interface ApiResponseInterface
{
    /**
     * Return a new JSON response from the application.
     *
     * @param string|array|null $data
     * @param int               $status
     * @param array             $headers
     * @param int               $options
     *
     * @return ApiResponseInterface
     */
    public static function make($data = [], int $status = 200, array $headers = [], int $options = 0): ApiResponseInterface;
}