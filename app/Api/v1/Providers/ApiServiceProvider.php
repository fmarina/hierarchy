<?php

namespace App\Api\v1\Providers;


use App\Api\v1\Managers\Employee\Hierarchy\HierarchyManagerInterface;
use App\Api\v1\Managers\Employee\Hierarchy\SimpleHierarchyManager;
use Illuminate\Support\ServiceProvider;

/**
 * Class ApiServiceProvider
 *
 * @package App\Api\v1\Providers
 * @codeCoverageIgnore
 */
class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HierarchyManagerInterface::class, SimpleHierarchyManager::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            HierarchyManagerInterface::class,
        ];
    }
}
