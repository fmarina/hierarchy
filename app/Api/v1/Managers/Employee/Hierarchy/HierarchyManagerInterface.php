<?php

namespace App\Api\v1\Managers\Employee\Hierarchy;

/**
 * Interface HierarchyManagerInterface
 *
 * @package App\Api\v1
 */
interface HierarchyManagerInterface
{
    /**
     * Build a hierarchy tree from a file
     *
     * @param string $filePath
     *
     * @return array
     * @throws HierarchyException
     */
    public function buildTreeFromFile(string $filePath): array;

    /**
     * Build a hierarchy tree from an array
     *
     * @param array $records
     *
     * @return array
     * @throws HierarchyException
     */
    public function buildTreeFromArray(array $records): array;
}