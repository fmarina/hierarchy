<?php

namespace App\Api\v1\Managers\Employee\Hierarchy;

use App\Api\v1\Managers\ManagerInterface;
use App\Api\v1\Traits\DecodesJson;


/**
 * Class SimpleHierarchyManager
 *
 * @package App\Api\v1\Managers
 */
class SimpleHierarchyManager implements ManagerInterface, HierarchyManagerInterface
{
    use DecodesJson;

    /**
     * SimpleHierarchyManager constructor.
     *
     */
    public function __construct() {
        //
    }

    /**
     * Build a hierarchy tree from a file
     *
     * @param string $filePath
     *
     * @return array
     * @throws HierarchyException
     */
    public function buildTreeFromFile(string $filePath): array
    {
        $records = $this->decodeJsonFromFile($filePath);

        // @codeCoverageIgnoreStart
        if (null === $records) {
            throw new HierarchyException('The file can not be read. Check your code here https://jsonlint.com/, for example.', true);
        }

        if (!\is_array($records) || \count($records) < 1) {
            throw new HierarchyException('The file should contain JSON code with at list one record.', true);
        }
        // @codeCoverageIgnoreEnd

        return $this->buildTreeFromArray($records);
    }

    /**
     * Build a hierarchy tree from an array
     *
     * @param array $records
     *
     * @return array
     * @throws HierarchyException
     */
    public function buildTreeFromArray(array $records): array
    {
        if (empty($records)) {
            throw new HierarchyException('There should be at list one record.', true);
        }

        try {
            $allEmployeeNames = array_unique(array_merge(array_keys($records), array_values($records)));
        } catch (\ErrorException $e) {
            throw new HierarchyException('Hey! The records should contain only key:value structure!', true);
        }

        // here we will collect hierarchy tree for each employee
        $employees = [];

        // this array will contain the hierarchy tree of the big Boss
        $hierarchyTree = [];

        foreach ($allEmployeeNames as $employeeName) {

            if (!isset($employees[$employeeName])) {
                $employees[$employeeName] = [];
            }

            // get supervisor's name for this employee
            $supervisorName = $records[$employeeName] ?? null;

            if (null === $supervisorName) {

                if (\count($hierarchyTree) > 0) {
                    throw new HierarchyException('Hey! There was supposed to be just one big Boss!', true);
                }

                // found the Boss
                $hierarchyTree[$employeeName] = &$employees[$employeeName];

            } else {
                // check if this supervisor was already defined as a dependent employee of this employee
                if ($this->isDependent($supervisorName, $employees[$employeeName])) {
                    throw new HierarchyException('Hey! No loops please!', true);
                }
            }

            // add this employee to the supervisor array
            $employees[$supervisorName][][$employeeName] = &$employees[$employeeName];
        }

        return $hierarchyTree;
    }

    /**
     * Search for the tested employee name among the dependent employees
     *
     * @param string $employeeName
     * @param array  $dependentEmployees
     *
     * @return bool
     */
    protected function isDependent(string $employeeName, array $dependentEmployees): bool
    {
        $isDependent = false;

        if (!empty($dependentEmployees)) {

            foreach ($dependentEmployees as $dependentEmployee) {

                // search for the tested employee name among dependent employees
                if (array_key_exists($employeeName, $dependentEmployee)) {
                    return true;
                }

                foreach ($dependentEmployee as $nextDependentEmployees) {
                    if (count($nextDependentEmployees) > 0) {

                        // if the tested employee has dependent employees, then let's check them too
                        $isDependent = $this->isDependent($employeeName, $nextDependentEmployees);

                        if ($isDependent) {
                            return true;
                        }
                    }
                }
            }
        }

        return $isDependent;
    }
}
