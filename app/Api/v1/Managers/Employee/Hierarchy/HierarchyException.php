<?php

namespace App\Api\v1\Managers\Employee\Hierarchy;

use App\Api\v1\Exceptions\ApiException;

/**
 * Exception HierarchyException
 *
 * @package App\Api\v1
 */
class HierarchyException extends ApiException
{
    //
}