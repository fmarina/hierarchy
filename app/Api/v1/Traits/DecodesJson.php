<?php

namespace App\Api\v1\Traits;

/**
 * Trait DecodesJson
 *
 * @package App\Api\v1\Traits
 */
trait DecodesJson
{
    /**
     * Decodes a json string into an array
     *
     * @param $fileContent
     *
     * @return array|mixed
     */
    public function decodeJson($fileContent)
    {
        return @json_decode(mb_convert_encoding($fileContent, 'UTF-8'), true);
    }

    /**
     * Decodes data from a file into an array
     *
     * @param string $filePath
     *
     * @return array|mixed
     */
    public function decodeJsonFromFile(string $filePath)
    {
        if (!file_exists($filePath)) {
            return null;
        }

        $fileContent = file_get_contents($filePath);

        // @codeCoverageIgnoreStart
        if (false === $fileContent) {
            return null;
        }
        // @codeCoverageIgnoreEnd

        return $this->decodeJson($fileContent);
    }
}