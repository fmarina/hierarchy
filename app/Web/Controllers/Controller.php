<?php

namespace App\Web\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 *
 * @package App\Web\Controllers
 */
class Controller extends BaseController
{
    //
}
