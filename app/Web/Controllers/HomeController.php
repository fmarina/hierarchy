<?php

namespace App\Web\Controllers;


use Illuminate\View\View;

/**
 * Class Controller
 *
 * @package App\Web\Controllers
 */
class HomeController extends Controller
{
    /**
     * Show the home page
     *
     * @return View
     */
    public function show(): View
    {
        return view('pages.home');
    }
}
