<?php

namespace App\Exceptions;

use App\Api\v1\Exceptions\ApiException;
use App\Api\v1\Responses\ApiResponse;
use App\Api\v1\Responses\ApiResponseInterface;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Handler
 *
 * @package App\Exceptions
 * @codeCoverageIgnore
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport
        = [
            ApiException::class,
            HttpException::class,
            ValidationException::class,
        ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     *
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception               $exception
     *
     * @return \Illuminate\Http\Response|JsonResponse|ApiResponseInterface
     */
    public function render($request, Exception $exception)
    {
        $status = Response::HTTP_INTERNAL_SERVER_ERROR;

        /**
         * Define the response status
         */
        if ($exception instanceof HttpResponseException) {
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        } else if ($exception instanceof ModelNotFoundException) {
            $status = Response::HTTP_NOT_FOUND;
        } else if ($exception instanceof MethodNotAllowedHttpException) {
            $status = Response::HTTP_METHOD_NOT_ALLOWED;
        } else if ($exception instanceof NotFoundHttpException) {
            $status = Response::HTTP_NOT_FOUND;
        } else if ($exception instanceof AuthorizationException) {
            $status = Response::HTTP_FORBIDDEN;
        } else if ($exception instanceof \Dotenv\Exception\ValidationException) {
            $status = Response::HTTP_BAD_REQUEST;
        } else if ($exception instanceof ValidationException) {
            $status = $exception->status;
            $errors = $exception->errors();
        } else if ($exception instanceof ApiException) {
            $status = $exception->getStatusCode();
            $errors = $exception->errors();
        }

        /**
         * Render the exception for web
         * - if request expects Json
         * - if some unusual for api error occurred and we are in debug mode
         */
        if (!$request->expectsJson() ||
            (
                env('APP_DEBUG') &&
                !(
                    $exception instanceof ValidationException ||
                    $exception instanceof ApiException
                )
            )
        ) {
            /**
             * If we have a dedicated template for this type of error
             * then let's return it.
             */
            if (app('view')->exists($view = "errors.{$status}")) {
                return response(view($view, ['exception' => $exception]), $status);
            }

            return parent::render($request, $exception);
        }

        /**
         * Response for an api request
         */
        return ApiResponse::make([
            'success' => false,
            'status'  => $status,
            'message' => $exception->getMessage(),
            'errors'  => $errors ?? [],
        ], $status);
    }
}
