/**
 * API methods
 *
 * @param {axios} req
 * @returns {*}
 */
let api = (req) => {

    return {

        employee: {
            uploadHierarchy (hierarchy_file) {
                return req.post(
                    'employee/hierarchy/file',
                    hierarchy_file,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                );
            }
        }

    };
};

export default api;
