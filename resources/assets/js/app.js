/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries.
 */

import Vue from 'vue';
import Axios from 'axios';
import api from './api';
import Hierarchy from './components/HierarchyComponent';

let baseApiUrl = window.location.origin + '/api/v1';

//create Axios instance
let axios = Axios.create({
    baseURL: baseApiUrl,
    headers: {
        'X-Requested-With': 'XMLHttpRequest'
    }
});

let apiMethods = api(axios);

Vue.prototype.$axios = axios;
Vue.prototype.$api = apiMethods;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page.
 */
const app = new Vue({
    el: '#app',

    data: {},

    components: {
        'hierarchy': Hierarchy
    }
});
