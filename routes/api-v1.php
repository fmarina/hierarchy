<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the API.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */
$router->post('/employee/hierarchy/file', 'EmployeeController@postUploadHierarchyFile');
