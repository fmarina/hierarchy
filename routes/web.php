<?php

/*
|--------------------------------------------------------------------------
| WEB Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the application.
|
*/

$router->get('/', 'HomeController@show');